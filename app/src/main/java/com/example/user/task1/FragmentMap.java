package com.example.user.task1;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentMap extends Fragment {
    View view;
    private TabLayout bottom2;

    public FragmentMap() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.map_fragment, container, false);

        TabLayout bottom2 = (TabLayout) view.findViewById(R.id.bottom2);

        //Setting up bottom tablayout
        bottom2.addTab(bottom2.newTab().setIcon(R.drawable.ic_search));
        bottom2.addTab(bottom2.newTab().setIcon(R.drawable.ic_location));
        bottom2.addTab(bottom2.newTab().setIcon(R.drawable.ic_zoom));
        return view;
    }
}

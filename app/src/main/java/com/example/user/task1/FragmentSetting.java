package com.example.user.task1;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentSetting extends Fragment {
    View view;
    private TabLayout bottom4;
    public FragmentSetting() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.setting_fragment, container, false);

        TabLayout bottom4 = (TabLayout) view.findViewById(R.id.bottom4);

        //Setting up bottom tablayout
        bottom4.addTab(bottom4.newTab().setIcon(R.drawable.ic_setting));
        bottom4.addTab(bottom4.newTab().setIcon(R.drawable.ic_security));
        bottom4.addTab(bottom4.newTab().setIcon(R.drawable.ic_wallpaper));

        return view;
    }
}

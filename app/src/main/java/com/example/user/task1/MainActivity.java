package com.example.user.task1;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.Tab;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import static com.example.user.task1.R.id.contact_id;

public class MainActivity extends AppCompatActivity {
    private TabLayout topTabLayout;
    private TabLayout bottomTablayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        topTabLayout = (TabLayout) findViewById(R.id.top_tablayout);
        bottomTablayout = (TabLayout) findViewById(R.id.bottom_tablayout);
        viewPager = (ViewPager) findViewById(R.id.viewpager_id);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());


        //Setting up the viewpager and tablayoutt
        viewPager.setAdapter(viewPagerAdapter);
        topTabLayout.setupWithViewPager(viewPager);
        setCustomFont();
    }

    private void setCustomFont() {
        ViewGroup vg =(ViewGroup) topTabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();

        for (int j = 0; j < tabsCount ; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);

            int tabChildsCount = vgTab.getChildCount();

            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(Typeface.createFromAsset(getAssets(), "fonts/brown_bold.ttf"));
                }
            }

        }
    }

}

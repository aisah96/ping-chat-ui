package com.example.user.task1;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


public class FragmentContact extends Fragment {
    View view;
    FragmentAddContact addContact;
    private TabLayout bottom;

    public FragmentContact() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.contact_fragment, container, false);

        TabLayout bottom = (TabLayout) view.findViewById(R.id.bottom);

        //Setting up bottom tablayout
        bottom.addTab(bottom.newTab().setIcon(R.drawable.ic_contact));
        bottom.addTab(bottom.newTab().setIcon(R.drawable.ic_add_person));
        bottom.addTab(bottom.newTab().setIcon(R.drawable.ic_person));
//        bottom.setOnClickListener(this);
        return view;
    }
//Nested Fragment
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        insertNestedFragment();
//    }
//
//    private void insertNestedFragment() {
//
//        Fragment chilsFragment = new FragmentAddContact();
//        FragmentTransaction transaction = getChildFragmentManager(). beginTransaction();
//        transaction.replace(R.id.frame_layout, chilsFragment);
//    }
//
//    @Override
//    public void onClick(View v) {
//
//    }
}


package com.example.user.task1;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class FragmentChat extends Fragment {
    View view;
    private TabLayout bottom1;
    public FragmentChat() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.chat_fragment, container, false);

        TabLayout bottom1 = (TabLayout) view.findViewById(R.id.bottom1);
        //LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
       // layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        //Setting up bottom tablayout
        bottom1.addTab(bottom1.newTab().setIcon(R.drawable.ic_chat));
        bottom1.addTab(bottom1.newTab().setIcon(R.drawable.ic_call));
        bottom1.addTab(bottom1.newTab().setIcon(R.drawable.ic_mic));
        return view;

    }
}

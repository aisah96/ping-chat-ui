package com.example.user.task1;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentFeed extends Fragment {
    View view;
    private TabLayout bottom3;
    public FragmentFeed() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.feed_fragment, container, false);

        TabLayout bottom3 = (TabLayout) view.findViewById(R.id.bottom3);

        //Setting up bottom tablayout
        bottom3.addTab(bottom3.newTab().setIcon(R.drawable.ic_comment));
        bottom3.addTab(bottom3.newTab().setIcon(R.drawable.ic_notification));

        return view;
    }
}

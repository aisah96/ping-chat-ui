package com.example.user.task1;

import android.graphics.drawable.Icon;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter{

    private final List<Fragment> fragmentList = new ArrayList<>();
    private final List<String> FragmentListTitles = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
            FragmentContact Contact = new FragmentContact();
            return Contact;
            case 1:
                FragmentChat Chat = new FragmentChat();
                return Chat;
            case 2:
                FragmentMap Map = new FragmentMap();
                return Map;
            case 3:
                FragmentFeed Feed = new FragmentFeed();
                return Feed;
            case 4:
                FragmentSetting Setting = new FragmentSetting();
                return Setting;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {

        return 5;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "CONTACT";
            case 1:
                return "CHAT";
            case 2:
                return "MAP";
            case 3:
                return "FEED";
            case 4:
                return "SETTING";
            default:
                return null;
        }
    }

//    public void AddFragment(Fragment fragment, String Title) {
//        fragmentList.add(fragment);
//        FragmentListTitles.add(Title);
//    }
//
//
}
